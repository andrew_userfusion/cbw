<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$plugin_info = array(  'pi_name' => 'Calculate Remainder',
    'pi_version' => '0.1',
    'pi_author' => 'User Fusion',
    'pi_author_url' => 'http://userfusion.com',
    'pi_description' => 'Calculates remaining payments.',
    'pi_usage' => Calculate_remainder::usage());

class Calculate_remainder{
	
	var $return_data;

    public function __construct()
    {
		$this->EE =& get_instance();
		//set PARAMS
		$order_id = $this->EE->TMPL->fetch_param('order_id');
		
		$this->EE->db->select('*');
		$this->EE->db->from('exp_store_order_items');
		$this->EE->db->where('order_id' , $order_id);
		$query = $this->EE->db->get();
		if ($query->num_rows() > 0)
		{
		$return = '<ul>';
			foreach($query->result() as $row)
			{
				
				$sku = $row->sku;
				$return .= '<li>'.$sku.'</li>';
				//get the item_total, entry_id (product_id), sku, stock_id (variation id);
			}
			$return .= '</ul>';
			$this->return_data =  $return; 
		}
		else
		{
		$this->return_data =  'Error: order not found';
		}
		
		return;
		 		

    }

   
   
	// ----------------------------------------
	//  Plugin Usage
	// ----------------------------------------
	 
	// This function describes how the plugin is used.
	//  Make sure and use output buffering
	 
	function usage()
	{
	ob_start(); 
	?>
	 
Plugin Instructions go here
	 
	<?php
	$buffer = ob_get_contents();
	 
	ob_end_clean(); 
	 
	return $buffer;
	}
	
	/* End */

}
/* END Class */
/* End of file pi.calculate_remainder.php */
/* Location: ./system/expressionengine/third_party/calculate_remainder/pi.calculate_remainder.php */