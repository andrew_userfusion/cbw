<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ExpressionEngine - by EllisLab
 *
 * @package		ExpressionEngine
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2003 - 2011, EllisLab, Inc.
 * @license		http://expressionengine.com/user_guide/license.html
 * @link		http://expressionengine.com
 * @since		Version 2.0
 * @filesource
 */
 
// ------------------------------------------------------------------------

/**
 * Full Total Ext Extension
 *
 * @package		ExpressionEngine
 * @subpackage	Addons
 * @category	Extension
 * @author		User Fusion
 * @link		
 */

class Full_total_ext {
	
	public $settings 		= array();
	public $description		= 'Updates order_total to the full total of the order';
	public $docs_url		= '';
	public $name			= 'Full Total Ext';
	public $settings_exist	= 'n';
	public $version			= '1.0';
	
	private $EE;
	
	/**
	 * Constructor
	 *
	 * @param 	mixed	Settings array or empty string if none exist.
	 */
	public function __construct($settings = '')
	{
		$this->EE =& get_instance();
		$this->settings = $settings;
	}// ----------------------------------------------------------------------
	
	/**
	 * Activate Extension
	 *
	 * This function enters the extension into the exp_extensions table
	 *
	 * @see http://codeigniter.com/user_guide/database/index.html for
	 * more information on the db class.
	 *
	 * @return void
	 */
	public function activate_extension()
	{
		// Setup custom settings in this array.
		$this->settings = array();
		
		$data = array(
			'class'		=> __CLASS__,
			'method'	=> 'on_store_order_complete_end',
			'hook'		=> 'store_order_complete_end',
			'settings'	=> serialize($this->settings),
			'version'	=> $this->version,
			'enabled'	=> 'y'
		);

		$this->EE->db->insert('extensions', $data);			
		
	}	

	// ----------------------------------------------------------------------
	
	/**
	 * 
	 *
	 * @param
	 * @return
	 */
	public function on_store_order_complete_end($order)
	{
		// Code to look for Hook activation
		if ($this->EE->extensions->active_hook('store_order_complete_end') === TRUE)
		{
			$this->EE->extensions->call('store_order_complete_end', $order);
		}

		// Declare total_price
		$total_price = 0;
		
		// Loop through each item in the order
		foreach ($order['items'] AS $item)
		{
			// Get entry_id of item
			$entry_id = $item['entry_id'];
			
			/* Use entry_id to get corresponding entry from products table
			 * Get the full price of the product
			 */
			$this->EE->db->select('price');
			$this->EE->db->from('exp_store_products');
			$this->EE->db->where('entry_id' , $entry_id);
			$query = $this->EE->db->get();
			
			if ($query->num_rows() > 0)
			{
				foreach($query->result() as $row)
				{
					$price = $row->price;
					$price *= $item['item_qty'];
					$total_price += $price;
				}
			}
			else
			{
				$this->return_data =  'Error: order not found';
			}
		}
		
		
		$data = array(
		   'order_total' => $total_price
		);
		
		$this->EE->db->where('id', $order['id']);
		$this->EE->db->update('exp_store_orders', $data); 
		
	}

	// ----------------------------------------------------------------------

	/**
	 * Disable Extension
	 *
	 * This method removes information from the exp_extensions table
	 *
	 * @return void
	 */
	function disable_extension()
	{
		$this->EE->db->where('class', __CLASS__);
		$this->EE->db->delete('extensions');
	}

	// ----------------------------------------------------------------------

	/**
	 * Update Extension
	 *
	 * This function performs any necessary db updates when the extension
	 * page is visited
	 *
	 * @return  mixed   void on update / false if none
	 */
	function update_extension($current = '')
	{
		if ($current == '' OR $current == $this->version)
		{
			return FALSE;
		}

		if ($current < '1.0')
		{
			// Update to version 1.0
		}

		$this->EE->db->where('class', __CLASS__);
		$this->EE->db->update(
					'extensions',
					array('version' => $this->version)
		);
	}
	
	// ----------------------------------------------------------------------
}

/* End of file ext.full_total_ext.php */
/* Location: /system/expressionengine/third_party/full_total_ext/ext.full_total_ext.php */